import Home from "./components/Home";

export const Routes = [
  {
    path: "/",
    component: Home,
    exact: true
  }
];
