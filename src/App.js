import React from "react";
import "./App.css";
import DashboardHome from "./components/Dashboard.Home";
import { BrowserRouter as Router } from "react-router-dom";

const App = props => {
  return (
    <Router>
      <DashboardHome />
    </Router>
  );
};

export default App;
