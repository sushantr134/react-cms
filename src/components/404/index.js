import React from "react";
import { Button, Result } from "antd";
import { Link } from "react-router-dom";

const ErrorPage = props => {
  return (
    <Result
      status={"404"}
      title={"404 Error"}
      subTitle={"Sorry, the page you visited is not found."}
      extra={
        <Link to={"/"}>
          <Button type={"primary"}>Go Home</Button>
        </Link>
      }
    />
  );
};

export default ErrorPage;