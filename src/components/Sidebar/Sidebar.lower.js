import React from "react";
import {SettingOutlined} from "@ant-design/icons";

import {Link} from 'react-router-dom';
const SidebarLower = props => {
  const Options = [
    {
      id: "1",
      name: "Feedback",
      link: "/"
    },
    {
      id: "2",
      name: "Privacy Policy",
      link: "/"
    },
    {
      id: "3",
      name: "Terms & Conditions",
      link: "/"
    }
  ];

  return (
    <div className="sideoptions lower">
      <label id="settings">
        <SettingOutlined style={{fontSize:"18px",color:"#656565eb !important"}} />
        &nbsp;&nbsp;Settings
      </label>
      <div className="line"></div>
      <ul>
        {Options.map(option => (
            <li key={option.id}><Link to={option.link}>&nbsp;{option.name}</Link></li>
        ))}
      </ul>
    </div>
  );
};

export default SidebarLower;
