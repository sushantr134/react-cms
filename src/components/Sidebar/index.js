import React from "react";
import CompanyLogo from "../../logo.svg";

import "./styles/style.scss";
import SidebarUpper from "./Sidebar.upper";
import SidebarLower from "./Sidebar.lower";

const Sidebar = props => {
  return (
    <section id="sidebar">
      <nav>
        <span>
          <img src={CompanyLogo} width="auto" height="40" alt="react" />
          __logo__
        </span>
      </nav>
      <SidebarUpper />
      <SidebarLower />
    </section>
  );
};

export default Sidebar;
