import React from "react";
import {
  PieChartOutlined,
  FileOutlined,
  AppstoreAddOutlined,
  NotificationOutlined,
  UsergroupAddOutlined,
  EditOutlined
} from "@ant-design/icons";

import {Link} from 'react-router-dom';

const SidebarUpper = props => {
  const Options = [
    {
      id: "1",
      name: "Dashboard",
      icon: <PieChartOutlined />,
      link: "/"
    },
    {
      id: "2",
      name: "New Campaign",
      icon: <FileOutlined />,
      link: "/NewCampaign"
    },
    {
      id: "3",
      name: "Profile Manager",
      icon: <AppstoreAddOutlined />,
      link: "/"
    },
    {
      id: "4",
      name: "My Campaigns",
      icon: <NotificationOutlined />,
      link: "/"
    },
    {
      id: "5",
      name: "Influencers",
      icon: <UsergroupAddOutlined />,
      link: "/"
    },
    {
      id: "6",
      name: "Drafts",
      icon: <EditOutlined />,
      link: "/"
    }
  ];

  return (
    <div className="sideoptions upper">
      <ul>
        {Options.map(option => {
          return (
            <li key={option.id}>
              <Link to={option.link}>
                {option.icon}&nbsp;&nbsp;{option.name}
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default SidebarUpper;
