import React from "react";
import Sidebar from "./Sidebar";
import { Routes } from "../Routes";
import { Switch, Route } from "react-router-dom";
import ErrorPage from "./404";

const DashboardHome = props => {
  return (
    <section id="main">
      <Sidebar />
      <Switch>
        {Routes.map(({ path, exact, component: C, ...rest }) => {
          return (
            <Route
              key={path}
              path={path}
              exact={exact}
              render={props => <C {...props} {...rest} />}
            />
          );
        })}
        <Route render={() => <ErrorPage/>} />
      </Switch>
    </section>
  );
};

export default DashboardHome;
