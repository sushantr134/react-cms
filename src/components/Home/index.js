import React from "react";
import { Badge, Input, Select, Tooltip, Avatar } from "antd";
import "./styles/style.scss";
import { Link, withRouter } from "react-router-dom";

import {
  BellOutlined,
  CaretDownOutlined,
  DownloadOutlined
} from "@ant-design/icons";

const { Search } = Input;
const { Option } = Select;

//need to look for naming for this variable
const dropDownOneItems = ["Sephoria Inc.", "Google Inc.", "Facebook Inc."];
const dropDownTwoItems = ["pdf", "xlsx", "zip"];
const dropDownThreeItems = ["Last 30 Days", "Last 10 Days", "Last Week"];
const Home = props => {
  return (
    <section id="dashboard">
      <div className={"upper-search"}>
        <div>
          <Search placeholder={"Search"} size={"middle"} allowClear={true} />
        </div>
        <div></div>
        <div className={"notification-container"}>
          <Link to={"notifications"}>
            <Tooltip placement={"top"} title={"You have 2 new notifications"}>
              <Badge count={2}>
                <BellOutlined style={{ fontSize: "22px", color: "grey" }} />
              </Badge>
            </Tooltip>
          </Link>
          <span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</span>
          <div className={"user-avatar-container"}>
            <div className={"user-details"}>
              <span id={"name"}>Sushant Singh</span>
              <br />
              <span id={"designation"}>Admin</span>
            </div>
            <div className={"user-pic"}>
              <Avatar
                size={40}
                src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
              />
            </div>
          </div>
        </div>
      </div>

      <div className={"dropdown-container"}>
        <div id={"dropdown-item-one"}>
          <Select
            defaultValue={dropDownOneItems[0]}
            style={{ width: 140 }}
            bordered={false}
            onChange={value => console.log(value)}
            suffixIcon={<CaretDownOutlined style={{ color: "grey" }} />}
          >
            {dropDownOneItems.map(itemName => (
              <Option key={itemName} value={itemName}>
                {itemName}
              </Option>
            ))}
          </Select>
        </div>
        <div id={"dropdown-item-two"}>
          <span>
            <DownloadOutlined style={{ color: "grey", fontSize: 15 }} />
          </span>
          <label>Download Report as</label>
          <Select
            defaultValue={""}
            style={{ width: 70, color: "black" }}
            bordered={false}
            onChange={value => console.log(value)}
            suffixIcon={<CaretDownOutlined style={{ color: "grey" }} />}
          >
            {dropDownTwoItems.map(itemName => (
              <Option key={itemName} value={itemName}>
                {itemName}
              </Option>
            ))}
          </Select>
        </div>

        <div id={"dropdown-item-three"}>
          <Select
            defaultValue={dropDownThreeItems[0]}
            style={{ width: 120 }}
            bordered={false}
            onChange={value => console.log(value)}
            suffixIcon={<CaretDownOutlined style={{ color: "grey" }} />}
          >
            {dropDownThreeItems.map(itemName => (
              <Option key={itemName} value={itemName}>
                {itemName}
              </Option>
            ))}
          </Select>
        </div>
      </div>
    </section>
  );
};

export default withRouter(Home);
